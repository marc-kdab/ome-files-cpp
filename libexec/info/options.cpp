/*
 * #%L
 * OME-FILES C++ library for image IO.
 * Copyright © 2014 - 2015 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2018 Quantitative Imaging Systems, LLC
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#include <info/options.h>

#ifdef OME_HAVE_BOOST_OPTIONS
namespace opt = boost::program_options;
#elif OME_HAVE_QT5_OPTIONS
#include <QtCore/QCoreApplication>
#include <sstream>
#endif

namespace info
{

#ifdef OME_HAVE_BOOST_OPTIONS
  options::options ():
#elif OME_HAVE_QT5_OPTIONS
  options::options (QCommandLineParser& parser):
#endif
    action(ACTION_METADATA),
    verbosity(MSG_NORMAL),
    showcore(true),
    showorig(true),
    filter(false),
    showomexml(false),
    validate(true),
    showsa(true),
    showused(true),
    merge(false),
    group(false),
    stitch(false),
    separate(false),
    series(0),
    resolution(0),
    format(),
    files(),
    inputOrderString(),
    outputOrderString(),
    inputOrder(),
    outputOrder(),
#ifdef OME_HAVE_BOOST_OPTIONS
    actions("Actions"),
    general("General options"),
    reader("Reader options"),
    metadata("Metadata filtering and display options"),
    hidden("Hidden options"),
    positional(),
    visible(),
    global(),
    vm()
#elif OME_HAVE_QT5_OPTIONS
    parser(&parser)
#endif
  {
  }

  options::~options ()
  {
  }

#ifdef OME_HAVE_BOOST_OPTIONS
  boost::program_options::options_description const&
  options::get_visible_options() const
  {
    return this->visible;
  }
#elif OME_HAVE_QT5_OPTIONS
  std::string
  options::get_visible_options() const
  {
    return parser->helpText().toStdString();
  }
#endif

#ifdef OME_HAVE_BOOST_OPTIONS
  void
  options::parse (int   argc,
                  char *argv[])
  {
    add_options();
    add_option_groups();

    opt::store(opt::command_line_parser(argc, argv).
               options(global).positional(positional).run(), vm);
    opt::notify(vm);

    check_options();
    check_actions();
  }
#elif OME_HAVE_QT5_OPTIONS
  void
  options::parse (const QCoreApplication& app)
  {
    add_options();

    this->parser->process(app);

    check_options();
    check_actions();
  }
#endif

  void
  options::add_options ()
  {
#ifdef OME_HAVE_BOOST_OPTIONS
    actions.add_options()
      ("usage,u",
       "Show command usage")
      ("help,h",
       "Display manual for this command")
      ("metadata",
       "Display image metadata (default)")
      ("version,V",
       "Print version information");
#elif OME_HAVE_QT5_OPTIONS
    parser->addOptions({
        {{"u", "usage"}, QCoreApplication::translate("main", "Show command usage")},
        {{"h", "help"}, QCoreApplication::translate("main", "Display manual for this command")},
        {"metadata", QCoreApplication::translate("main", "Display image metadata (default)")},
        {{"V", "version"}, QCoreApplication::translate("main", "Print version information")}
      });
#endif

#ifdef OME_HAVE_BOOST_OPTIONS
    general.add_options()
      ("debug",
       "Show debug output")
      ("quiet,q",
       "Show less output")
      ("verbose,v",
       "Show more output");
#elif OME_HAVE_QT5_OPTIONS
    parser->addOptions({
        {"debug", QCoreApplication::translate("main", "Show debug output")},
        {{"q", "quiet"}, QCoreApplication::translate("main", "Show less output")},
        {{"v", "verbose"}, QCoreApplication::translate("main", "Show more output")}
      });
#endif

#ifdef OME_HAVE_BOOST_OPTIONS
    reader.add_options()
      ("format", opt::value<std::string>(&this->format),
       "Use the specified format reader")
      ("merge", "Combine separate channels into an RGB image")
      ("no-merge", "Do not combine separate channels into an RGB image (default)")
      ("group", "Files in multi-file datasets are grouped as a single dataset")
      ("no-group", "Files in multi-file datasets are treated as a multiple datasets (default)")
      ("stitch", "Group files with similar names")
      ("no-stitch", "Do not group files with similar names (default)")
      ("separate", "Separate RGB image into separate channels")
      ("no-separate", "Do not separate RGB image into separate channels (default)")
      ("series", opt::value<ome::files::dimension_size_type>(&this->series),
       "Use the specified series")
      ("resolution", opt::value<ome::files::dimension_size_type>(&this->resolution),
       "Use the specified subresolution")
      ("input-order",
       opt::value<std::string>(&this->inputOrderString),
       "Override the dimension input order")
      ("output-order",
       opt::value<std::string>(&this->outputOrderString),
       "Override the dimension output order");
#elif OME_HAVE_QT5_OPTIONS
    parser->addOptions({
        {"format", QCoreApplication::translate("main", "Use the specified format reader"), "format"},
        {"merge", QCoreApplication::translate("main", "Combine separate channels into an RGB image")},
        {"no-merge", QCoreApplication::translate("main", "Do not combine separate channels into an RGB image (default)")},
        {"group", QCoreApplication::translate("main", "Files in multi-file datasets are grouped as a single dataset")},
        {"no-group", QCoreApplication::translate("main", "Files in multi-file datasets are treated as a multiple datasets (default)")},
        {"stitch", QCoreApplication::translate("main", "Group files with similar names")},
        {"no-stitch", QCoreApplication::translate("main", "Do not group files with similar names (default)")},
        {"separate", QCoreApplication::translate("main", "Separate RGB image into separate channels")},
        {"no-separate", QCoreApplication::translate("main", "Do not separate RGB image into separate channels (default)")},
        {"series", QCoreApplication::translate("main", "Use the specified series"), "series"},
        {"resolution", QCoreApplication::translate("main", "Use the specified subresolution"), "resolution"},
        {"input-order", QCoreApplication::translate("main", "Override the dimension input order"), "order"},
        {"output-order", QCoreApplication::translate("main", "Override the dimension output order"), "order"}
      });
#endif

#ifdef OME_HAVE_BOOST_OPTIONS
    metadata.add_options()
      ("core", "Display core metadata (default)")
      ("no-core", "Do not display core metadata")
      ("orig", "Display original format-specific global and series metadata (default)")
      ("no-orig", "Do not display original format-specific global and series metadata")
      ("filter", "Filter format-specific global and series metadata")
      ("no-filter", "Do not filter format-specific global and series metadata (default)")
      ("omexml", "Display OME-XML metadata")
      ("no-omexml", "Do not display OME-XML metadata (default)")
      ("validate", "Validate OME-XML metadata (default)")
      ("no-validate", "Do not validate OME-XML metadata")
      ("sa", "Display structured annotations (default)")
      ("no-sa", "Do not display structured annotations")
      ("used", "Display used files (default)")
      ("no-used", "Do not display used files");
#elif OME_HAVE_QT5_OPTIONS
    parser->addOptions({
        {"core", QCoreApplication::translate("main", "Display core metadata (default)")},
        {"no-core", QCoreApplication::translate("main", "Do not display core metadata")},
        {"orig", QCoreApplication::translate("main", "Display original format-specific global and series metadata (default)")},
        {"no-orig", QCoreApplication::translate("main", "Do not display original format-specific global and series metadata")},
        {"filter", QCoreApplication::translate("main", "Filter format-specific global and series metadata")},
        {"no-filter", QCoreApplication::translate("main", "Do not filter format-specific global and series metadata (default)")},
        {"omexml", QCoreApplication::translate("main", "Display OME-XML metadata")},
        {"no-omexml", QCoreApplication::translate("main", "Do not display OME-XML metadata (default)")},
        {"validate", QCoreApplication::translate("main", "Validate OME-XML metadata (default)")},
        {"no-validate", QCoreApplication::translate("main", "Do not validate OME-XML metadata")},
        {"sa", QCoreApplication::translate("main", "Display structured annotations (default)")},
        {"no-sa", QCoreApplication::translate("main", "Do not display structured annotations")},
        {"used", QCoreApplication::translate("main", "Display used files (default)")},
        {"no-used", QCoreApplication::translate("main", "Do not display used files")}
      });
#endif

#ifdef OME_HAVE_BOOST_OPTIONS
    hidden.add_options()
      ("files", opt::value<std::vector<std::string>>(&this->files),
       "Files to read");
#endif

#ifdef OME_HAVE_BOOST_OPTIONS
    positional.add("files", -1);
#elif OME_HAVE_QT5_OPTIONS
    parser->addPositionalArgument("files", QCoreApplication::translate("main", "Files to read"), "[files...]");
#endif
  }

#ifdef OME_HAVE_BOOST_OPTIONS
  void
  options::add_option_groups ()
  {
#ifndef BOOST_PROGRAM_OPTIONS_DESCRIPTION_OLD
    if (!actions.options().empty())
#else
      if (!actions.primary_keys().empty())
#endif
        {
          global.add(actions);
          visible.add(actions);
        }
#ifndef BOOST_PROGRAM_OPTIONS_DESCRIPTION_OLD
    if (!general.options().empty())
#else
      if (!general.primary_keys().empty())
#endif
        {
          global.add(general);
          visible.add(general);
        }
#ifndef BOOST_PROGRAM_OPTIONS_DESCRIPTION_OLD
    if (!reader.options().empty())
#else
      if (!reader.primary_keys().empty())
#endif
        {
          global.add(reader);
          visible.add(reader);
        }
#ifndef BOOST_PROGRAM_OPTIONS_DESCRIPTION_OLD
    if (!metadata.options().empty())
#else
      if (!metadata.primary_keys().empty())
#endif
        {
          global.add(metadata);
          visible.add(metadata);
        }
#ifndef BOOST_PROGRAM_OPTIONS_DESCRIPTION_OLD
    if (!hidden.options().empty())
#else
      if (!hidden.primary_keys().empty())
#endif
        global.add(hidden);
  }
#endif

  void
  options::check_options ()
  {
#ifdef OME_HAVE_BOOST_OPTIONS
    if (vm.count("usage"))
      this->action = ACTION_USAGE;

    if (vm.count("help"))
      this->action = ACTION_HELP;

    if (vm.count("version"))
      this->action = ACTION_VERSION;
#elif OME_HAVE_QT5_OPTIONS
    if (parser->isSet("usage"))
      this->action = ACTION_USAGE;

    if (parser->isSet("help"))
      this->action = ACTION_HELP;

    if (parser->isSet("version"))
      this->action = ACTION_VERSION;
#endif

#ifdef OME_HAVE_BOOST_OPTIONS
    if (vm.count("quiet"))
      this->verbosity = MSG_QUIET;
    if (vm.count("verbose"))
      this->verbosity = MSG_VERBOSE;
    if (vm.count("debug"))
      this->verbosity = MSG_DEBUG;
#elif OME_HAVE_QT5_OPTIONS
    if (parser->isSet("quiet"))
      this->verbosity = MSG_QUIET;
    if (parser->isSet("verbose"))
      this->verbosity = MSG_VERBOSE;
    if (parser->isSet("debug"))
      this->verbosity = MSG_DEBUG;
#endif

#ifdef OME_HAVE_BOOST_OPTIONS
    if (vm.count("merge"))
      this->merge = true;
    if (vm.count("no-merge"))
      this->merge = false;

    if (vm.count("group"))
      this->group = true;
    if (vm.count("no-group"))
      this->group = false;

    if (vm.count("stitch"))
      this->stitch = true;
    if (vm.count("no-stitch"))
      this->stitch = false;

    if (vm.count("separate"))
      this->separate = true;
    if (vm.count("no-separate"))
      this->separate = false;
#elif OME_HAVE_QT5_OPTIONS
    if (parser->isSet("merge"))
      this->merge = true;
    if (parser->isSet("no-merge"))
      this->merge = false;

    if (parser->isSet("group"))
      this->group = true;
    if (parser->isSet("no-group"))
      this->group = false;

    if (parser->isSet("stitch"))
      this->stitch = true;
    if (parser->isSet("no-stitch"))
      this->stitch = false;

    if (parser->isSet("separate"))
      this->separate = true;
    if (parser->isSet("no-separate"))
      this->separate = false;
#endif

#ifdef OME_HAVE_BOOST_OPTIONS
    if(!this->inputOrderString.empty())
      this->inputOrder = ome::xml::model::enums::DimensionOrder(inputOrderString);
    if(!this->outputOrderString.empty())
      this->outputOrder = ome::xml::model::enums::DimensionOrder(outputOrderString);
#elif OME_HAVE_QT5_OPTIONS
#endif

#ifdef OME_HAVE_BOOST_OPTIONS
    if (vm.count("core"))
      this->showcore = true;
    if (vm.count("no-core"))
      this->showcore = false;

    if (vm.count("orig"))
      this->showorig = true;
    if (vm.count("no-orig"))
      this->showorig = false;

    if (vm.count("filter"))
      this->filter = true;
    if (vm.count("no-filter"))
      this->filter = false;

    if (vm.count("omexml"))
      this->showomexml = true;
    if (vm.count("no-omexml"))
      this->showomexml = false;

    if (vm.count("validate"))
      this->validate = true;
    if (vm.count("no-validate"))
      this->validate = false;
#elif OME_HAVE_QT5_OPTIONS
    if (parser->isSet("core"))
      this->showcore = true;
    if (parser->isSet("no-core"))
      this->showcore = false;

    if (parser->isSet("orig"))
      this->showorig = true;
    if (parser->isSet("no-orig"))
      this->showorig = false;

    if (parser->isSet("filter"))
      this->filter = true;
    if (parser->isSet("no-filter"))
      this->filter = false;

    if (parser->isSet("omexml"))
      this->showomexml = true;
    if (parser->isSet("no-omexml"))
      this->showomexml = false;
#endif

#ifdef OME_HAVE_BOOST_OPTIONS
    if (vm.count("sa"))
      this->showsa = true;
    if (vm.count("no-sa"))
      this->showsa = false;
#elif OME_HAVE_QT5_OPTIONS
    if (parser->isSet("sa"))
      this->showsa = true;
    if (parser->isSet("no-sa"))
      this->showsa = false;
#endif

#ifdef OME_HAVE_BOOST_OPTIONS
    if (vm.count("used"))
      this->showused = true;
    if (vm.count("no-used"))
      this->showused = false;
#elif OME_HAVE_QT5_OPTIONS
    if (parser->isSet("used"))
      this->showused = true;
    if (parser->isSet("no-used"))
      this->showused = false;
#endif

    // Boost already handled options with arguments; Qt needs handling
    // specially.
#if OME_HAVE_QT5_OPTIONS
    if (parser->isSet("format"))
      this->format = parser->value("format").toStdString();
    if (parser->isSet("series"))
    {
      std::istringstream is(parser->value("series").toStdString());
      ome::files::dimension_size_type series;
      is >> series;
      if (!is)
      {
        throw std::runtime_error("Failed to parse series argument");
      }
    }
    if (parser->isSet("resolution"))
    {
      std::istringstream is(parser->value("resolution").toStdString());
      ome::files::dimension_size_type resolution;
      is >> resolution;
      if (!is)
      {
        throw std::runtime_error("Failed to parse resolution argument");
      }
    }
    if (parser->isSet("input-order"))
      this->inputOrderString = parser->value("input-order").toStdString();
    if (parser->isSet("output-order"))
      this->outputOrderString = parser->value("output-order").toStdString();

    QStringList flist = parser->positionalArguments();
    for (int i = 0; i < flist.size(); ++i)
      this->files.emplace_back(flist.at(i).toStdString());
#endif
  }

  void
  options::check_actions ()
  {
  }

}
