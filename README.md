# OME Files C++

[![pipeline status](https://gitlab.com/codelibre/ome/ome-files-cpp/badges/master/pipeline.svg)](https://gitlab.com/codelibre/ome/ome-files-cpp/commits/master)

OME Files is a standalone C++ library for reading and writing life
sciences image file formats.  It is the reference implementation of
the
[OME-TIFF](https://codelibre.gitlab.io/ome/ome-model/doc/ome-tiff/index.html)
file format.

This project was originally developed by the Open Microscopy
Environment in the [ome-common-cpp GitHub
repository](https://github.com/ome/ome-common-cpp).  The original
repository has been archived and is not currently under active
development.  This GitLab project is a fork, providing a continuation
of the original project, and is maintained by Codelibre Consulting
Limited.

Documentation
-------------

- [C++ API reference](https://codelibre.gitlab.io/ome/ome-files-cpp/api/)
- [Documentation](https://codelibre.gitlab.io/ome/ome-files-cpp/doc/)
- [Tutorial](https://codelibre.gitlab.io/ome/ome-files-cpp/doc/tutorial.html)
- [Performance testing](https://gitlab.com/codelibre/ome/ome-files-performance)

Purpose
-------

OME Files' primary purpose is to convert proprietary microscopy data
into an open standard called the OME data model, particularly into the
OME-TIFF file format. See the [statement of
purpose](https://docs.openmicroscopy.org/latest/bio-formats/about/index.html)
for a thorough explanation and rationale.  OME Files provides support
for reading and writing files using the OME-TIFF file format and for
the OME metadata model which is the basis for the file format.

Supported formats
-----------------

[OME-TIFF](https://codelibre.gitlab.io/ome/ome-model/doc/ome-tiff/index.html) using the OME [metadata model](https://gitlab.com/codelibre/ome/ome-model).

For users
---------

[Many software
packages](https://docs.openmicroscopy.org/latest/bio-formats/users/index.html)
use Bio-Formats to read and write open microscopy formats such as
OME-TIFF in Java programs.  OME Files provides equivalent
functionality for C++ programs.


For developers
--------------

You can use OME Files C++ to easily support reading and writing
OME-TIFF in your software.

Development
-----------

Feature requests, bug reports and improvements should be submitted as
issues or merge requests against this repository.

Codelibre Consulting Limited provides software development consulting
services for this and other projects.  Please [contact
us](mailto:consulting@codelibre.net) to discuss your requirements.

Merge request testing
---------------------

We welcome merge requests from anyone.

Please verify the following before submitting a pull request:

 * verify that the branch merges cleanly into `master`
 * verify that the GitLab pipeline passes
 * verify that the branch only uses C++14 features (this should
   be tested by the pipeline)
 * make sure that your commits contain the correct authorship information and,
   if necessary, a signed-off-by line
 * make sure that the commit messages or pull request comment contains
   sufficient information for the reviewer to understand what problem was
   fixed and how to test it
