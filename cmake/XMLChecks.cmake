# #%L
# OME C++ libraries (cmake build infrastructure)
# %%
# Copyright © 2018 Quantitative Imaging Systems, LLC
# %%
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are
# those of the authors and should not be interpreted as representing official
# policies, either expressed or implied, of any organization.
# #L%

if (POLICY CMP0067)
    cmake_policy(SET CMP0067 NEW)
endif()
if (POLICY CMP0075)
    cmake_policy(SET CMP0075 NEW)
endif()

include(CheckCXXSourceRuns)

set(xmldom "xerces;qt5" CACHE STRING "Use XML DOM classes types from 'xerces' (xerces and xalan) or 'qt5'.  Multiple options will be used as fallbacks")

set(OME_XML_DOM NOTFOUND)
set(OME_XML_XSLT NOTFOUND)

message(STATUS "Checking XML DOM and XSLT implementations: ${xmldom}")

foreach(xd ${xmldom})
    if (xd STREQUAL "xerces")
        find_package(XercesC 3.0.0)
        if (XercesC_FOUND)
            # Xerces-C++ link test
            set(CMAKE_REQUIRED_INCLUDES_SAVE ${CMAKE_REQUIRED_INCLUDES})
            set(CMAKE_REQUIRED_INCLUDES ${CMAKE_REQUIRED_INCLUDES} ${XercesC_INCLUDE_DIRS})
            set(CMAKE_REQUIRED_LIBRARIES_SAVE ${CMAKE_REQUIRED_LIBRARIES})
            set(CMAKE_REQUIRED_LIBRARIES ${CMAKE_REQUIRED_LIBRARIES} ${XercesC_LIBRARIES})

            check_cxx_source_runs(
                "#include <xercesc/util/PlatformUtils.hpp>

int main() {
  xercesc::XMLPlatformUtils::Initialize();
  xercesc::XMLPlatformUtils::Terminate();
}"
                    XERCES_LINK)

            if(NOT XERCES_LINK)
                message(WARNING "Xerces-C++ library link test failed")
            endif(NOT XERCES_LINK)

            set(CMAKE_REQUIRED_LIBRARIES ${CMAKE_REQUIRED_LIBRARIES_SAVE})
            set(CMAKE_REQUIRED_INCLUDES ${CMAKE_REQUIRED_INCLUDES_SAVE})
        endif()

        if (XercesC_FOUND AND XERCES_LINK)
            set(OME_XML_DOM TRUE)
            set(OME_HAVE_XERCES_DOM TRUE)
            message(STATUS "Using XML DOM: xerces")

            find_package(XalanC 1.10)

            if (XalanC_FOUND)
                # Xalan-C++ link test
                set(CMAKE_REQUIRED_INCLUDES_SAVE ${CMAKE_REQUIRED_INCLUDES})
                set(CMAKE_REQUIRED_INCLUDES ${CMAKE_REQUIRED_INCLUDES} ${XalanC_INCLUDE_DIRS})
                set(CMAKE_REQUIRED_LIBRARIES_SAVE ${CMAKE_REQUIRED_LIBRARIES})
                set(CMAKE_REQUIRED_LIBRARIES ${CMAKE_REQUIRED_LIBRARIES} ${XalanC_LIBRARIES})

                check_cxx_source_runs(
                    "#include <xercesc/util/PlatformUtils.hpp>
#include <xalanc/XalanTransformer/XalanTransformer.hpp>

int main()
{
  xercesc::XMLPlatformUtils::Initialize();
  xalanc::XalanTransformer::initialize();
  xalanc::XalanTransformer::terminate();
  xercesc::XMLPlatformUtils::Terminate();
}
"
                        XALAN_LINK)

                if(NOT XALAN_LINK)
                    message(WARNING "Xalan-C++ library link test failed")
                endif(NOT XALAN_LINK)

                set(CMAKE_REQUIRED_LIBRARIES ${CMAKE_REQUIRED_LIBRARIES_SAVE})
                set(CMAKE_REQUIRED_INCLUDES ${CMAKE_REQUIRED_INCLUDES_SAVE})
            endif()

            if (XalanC_FOUND AND XALAN_LINK)
                set(OME_XML_XSLT TRUE)
                set(OME_HAVE_XALAN_XSLT TRUE)
                message(STATUS "Using XML XSLT: xalan")
            endif()
        endif()
    endif()
    if (xd STREQUAL "qt5")
        find_package(Qt5Xml)
        if (TARGET Qt5::Xml)
            set(OME_XML_DOM TRUE)
            set(OME_HAVE_QT5_DOM TRUE)
            message(STATUS "Using XML DOM: qt5")
        endif()
        find_package(Qt5XmlPatterns)
        if (TARGET Qt5::XmlPatterns)
            set(OME_XML_XSLT TRUE)
            set(OME_HAVE_QT5_XSLT TRUE)
            message(STATUS "Using XML XSLT: qt5")
        endif()
    endif()
    if(OME_XML_DOM)
        break()
    endif()
endforeach()

if(NOT OME_XML_DOM)
    message(FATAL_ERROR "No XML DOM implementation found")
endif()
