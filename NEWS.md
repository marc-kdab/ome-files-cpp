# 0.7.0

Unreleased.

## Library changes

* Remove use of Boost.Units:
  * Remove use of basic units in the UnitTypes library, which has been
    removed.
  * Remove use of Quantity unit conversion functionality in the
    ome-xml library.

# 0.6.0

Released on the 8th June 2019.

## Overview

Major changes include:

* OME-TIFF support for sub-resolutions.
* Removal of dependencies on Boost libraries (Boost header-only
  libraries are still required).
* Optional replacement of Xerces-C++ with Qt5Xml.
* Optional replacement of Xalan-C++ with Qt5XmlPatterns.

## Platform support

* C++14 is now the minimum required language version, with C++17 being
  used optionally when available (!93, !96).
* Microsoft Visual Studio 2017 and 2019 are now both supported.
* LLVM 6 is now supported.
* GCC 9 is now supported in C++14 mode; C++17 is currently unsupported
  since this release changed the type of
  `std::filesystem::file_time_type` returned by
  `std::filesystem::last_write_time()`, which requires non-portable
  conversion to `std::chrono::system_clock::time_point`.  This will be
  worked around in a future release, or with C++20 which will
  introduce portable time point conversion functions.

## Library changes

* Support for reading and writing sub-resolutions (!109, !110, !111)
* Improve handling of missing `TiffData` planes (!124).
* Fix for handling companion files (!137).
* Removal of deprecated and unused features (!114, !115, !129).
* Boost library removal (!113, !122, !123).
* Replacement of Boost.ProgramOptions with Qt options parsing (!131).
* Xerces dependency can be optionally replaced by Qt5Xml (!119, !130,
  !136).
* Xalan dependency can be optionally replaced by Qt5XmlPatterns (!118,
  !126, !130).
* Use separate OME UnitTypes library (!117).
* Updated CMake exported library configuration to handle optional
  Boost and Qt5 exports (!120, !127).

## Infrastructure changes

* Continuous integration testing is performed on GitLab, testing on
  FreeBSD, Linux, MacOS X and Windows platforms.
  (!102, !112, !125, !133, !135).
* Documentation link updates for GitLab migration
  (!121).

# 0.5.0

Released on the 1st December 2017.

## Overview

The main change in this release is the re-addition of compatibility
code to permit building on CentOS7 and RHEL7 with EPEL.  Other work
includes the switch to C++11 threads and mutexes from the Boost
equivalents, and preliminary CMake support for building with C++17
compilers.

## Source changes

* Use CMP0067 to enable standard setting in feature tests.
* CMake 3.4 is the minimum version.
* Use C++11 `<thread>` and `<mutex>`.
* Restore support for Boost 1.53.
* Add support for Boost 1.65 and 1.65.1.
* Drop embedded copies of CMake `FindBoost`, `FindXalanC` and
  `FindXercesC`.
* Only run thread tests with CMake ≥3.8.
* Add tiling performance benchmark documentation.
* Add missing implicit `<cstdlib>` and `<iostream>` includes.
* Remove `REQUIRED` from exported cmake configuration.

# 0.4.0

Released on the 30th June 2017.

## Overview

The major changes in this release are the addition of tiling support
to the writer interface and the OME-TIFF writer, and the addition of
examples for use of the metadata and model API for adding extended
metadata, including annotations.

## Source changes

* Add support for tiling to `FormatWriter`.
* `FormatTools::getZCTCoords()` checks input more strictly, and throws
  `std::out_of_range` when out of range.
* Add documentation examples for tiling, using the metadata and model
  APIs to add extended metadata, annotations and link annotations.
* Use C++11 deleted methods to replace private unimplemented methods.
* Minor build improvements.

# 0.3.2

Released on the 23rd February 2017.

## Overview

This release contains minor bugfixes only.

## Source changes

* `MinimalTIFFWriter`: align interleaved setting and
  `PlanarConfiguration` TIFF tag with the `OMETIFFWriter` behaviour.

# 0.3.1

Released on the 17th February 2017.

## Overview

This release contains minor bugfixes only.

## Source changes

* MetadataTools: Resolve references after updating model objects.
* Add pixel buffer interleaving example.
* Update copyright dates.

# 0.3.0

Released on the 10th February 2017.

## Overview

The major changes in this release are:

* Updated the minimum C++ language standard to C++11 (from C++98).
* Added support for TIFF compression.
* Performance improvements for metadata reading and TIFF reading and
  writing.
* Usage of the `ome-model` component, replacing bioformats' `ome-xml`
  component (this is the data model specification and ome-xml library
  following decoupling from the Bio-Formats repository).

## Upgrading

The main change is the switch to C++11. No immediate source changes
are strictly required; the compatibility headers will continue to work
for now, however upgrading to the standard C++11 headers is highly
recommended to avoid future breakage. Client code is free to use any
C++11 features of their choosing. To update your code:

* Enable C++11 or later support for your compiler.
  * MSVC users do not need to do anything (it doesn't have any special
    option).
  * GCC and Clang users need to add the `-std=c++11` or `-std=c++14`
    option (the most recent versions default to C++14 and don't need
    this).
  * CMake users can set `CMAKE_CXX_STANDARD_REQUIRED=11` and
    `CMAKE_CXX_STANDARD=14` to try C++14 and fall back to C++11 if
    C++14 is unavailable.
* Replace `<ome/compat/array.h>` with `<array>` and use of
  `ome::compat::array` with `std::array`
* Replace `<ome/compat/cstdint.h>` with `<cstdint>`.
* Replace `<ome/compat/memory.h>` with `<memory>` and use of
  `ome::compat::shared_ptr` with `std::shared_ptr` (likewise for
  related types and functions such as `std::weak_ptr`,
  `std::make_shared` and `std::dynamic_pointer_cast`).
* Replace `<ome/compat/tuple.h>` with `<tuple>` and use of
  `ome::compat::tuple` with `std::tuple` (likewise for related
  functions such as `std::get`).
* The above compatibility headers, and their declared types in the
  ome::compat namespace still exist but are deprecated; they simply
  wrap the standard headers and types.
* The deprecated headers will be removed for 0.4.0.

## Source changes

* C++11 is now the minimum required language version, with C++14 being
  used when available.
  * Enabled the use of a restricted number of C++11 features,
    including `enum class`, `nullptr`, initializer lists, range-based for
    loops and type traits.
  * Enabled the use of C++11 syntax changes including `<::` not being
    interpreted as a trigraph and `>>` being used to close nested
    templates instead of `> >`.
  * Additional C++11 features will be enabled in subsequent releases.
* Google Test (gtest) is no longer built separately in each source
  component; the latest gtest release now allows use as a conventional
  library.
* Source releases are now made directly from git with `git archive`;
  additional version metadata is no longer embedded in the source
  releases.
* Added support for TIFF compression, exposing all compression
  algorithms supported by libtiff. Deflate and LZW will work for all
  pixel types; JPEG support will be available if libtiff was compiled
  with support for libjpeg.
* `OMETIFFReader` now sets the interleaved property according to the
  TIFF `PlanarConfiguration` tag; previously it was set to false
  unconditionally
* TIFF wrapper: Cache IFD offsets on reading; this speeds up reading
  by changing the complexity of indexed IFD access from O(n) to O(1).
* `OMETIFFWriter` sets the interleaved property unconditionally, even
  when the samples per pixel count is 1; this is to ensure easy
  round-tripping of data between readers and writers using the same
  logical dimension ordering in the pixel buffer.
* All TIFF writers: Set a default strip size of 2^16 pixels, which is
  for example 64KiB for 8-bit greyscale data, or 192KiB for 8-bit RGB
  data; the previous default was to copy the Java behavior of one row
  per strip, which was much less efficient.

# 0.2.3

Released on the 28th October 2016.

## Overview

This release contains minor bugfixes only.

## Source changes

* Update OME-TIFF reader handling of `Plane` elements with missing
  `TheZ`, `TheT` or `TheC` attributes to match the Java OME-TIFF
  reader.

# 0.2.2

Released on the 4th October 2016.

## Overview

The major focus of this release is to extend the support for features
in older OME-XML model versions and to improve the support for
automated testing all OME-XML and OME-TIFF features. This will
continue for the following release.

Major changes include:

* OME-TIFF reader and writer updates.

## Source changes

* Remove support for libtiff versions older than 4.0.3.
* Support invocation of the `ome-files` command via a symlink, to
  allow packaging for MacOS X with homebrew.
* Correct TIFF `COLORMAP` and `TRANSFERFUNCTION` handling; the colour
  maps are used by the `(get|set)(8|16)BitLookupTable()` methods in
  `FormatReader` and `FormatWriter` for the TIFF and OME-TIFF file
  formats.
* Boost updates (same as OME Common).

# 0.2.1

Released on the 15th September 2016.

## Overview

The major focus of this release was to port the most recent changes
from the OME-TIFF reader and writer from Bio-Formats Java to OME Files
C++. This means that the support for reading broken, non-standard and
complex OME-XML and OME-TIFF files is much improved. It is now
possible to read all OME-TIFF metadata using the `2007-06` schema and
above.

Major changes include:

* OME-TIFF reader and writer updates.

## Source changes

* Add units and quantities examples and tutorial.
* Document all namespaces in API reference.
* Update `OMETIFFReader` and `OMETIFFWriter` to match current
  Bio-Formats Java behaviour:
  * Read `BinaryOnly` companion metadata from TIFF or XML companion
    file; previously required an XML file.
  * Reduce the number of times the OME-XML metadata is parsed by
    caching the parsed metadata.
  * Cache the status of the existence and validity of TIFF files, to
    better handle incomplete or corrupt datasets by skipping repeated
    processing of known bad or nonexistent data
  * Add additional logging of TIFF read failures.
  * Add additional validity checks of `BinaryOnly` metadata and for
    `isThisType()` checks.
* Always remove old `TiffData` elements when writing new OME-TIFF planes.

# 0.2.0

Released on the 8th July 2016.

## Overview

The major focus of this release was to upgrade from the old `2013-06`
data model to the newly-released `2016-06` model. This brings with it
support for all of the `2015-01` model features such as map
annotations, units and quantities, plus new `2016-06` features such as
support for folders for images and ROIs, updated ROI model and updated
`LightSource` and `Shape` element hierarchies. The other major feature
is the addition of full support for Visual Studio 2015.

Major changes include:

* Added `2016-06` model support to replace the older `2013-06` model
  support.
* Added support for Visual Studio 2015, including binary builds.
* Improvements to the exported CMake configuration to make using the
  libraries easier.
* Added a BSD licence header to all components.
* The `ome-files` command wrapper now works on Windows.
* Documentation reworked to include cross-references between the
  manuals and API references within each component.

## Source changes

* Reworked the documentation including removal of the CMake
  Super-Build documentation and automatic checking of links.
* The `ome-files` command wrapper now works on Windows.
* Added MSVC workarounds for system macros with names matching unit
  names.
* Removed VS2012 workarounds.
* Improved CMake configuration export.
* Added BSD licence text.

# 0.1.1

Released on the 6th April 2016.

## Overview

Major changes include:

* Handling transform failure gracefully.

## Source changes

* Correct modification times of generated files in source archive.
* Add unit tests for non-square TIFF files.
* Correct names of manual pages following repository rename.
* Correct line ending style of generated CMake configuration script.

# 0.1.0

Released on the 21st March 2016.

## Overview

OME Files C++ was previously named "Bio-Formats C++" and was part of
the Bio-Formats Java source repository. With this initial release of
OME Files, the C++ source code has been moved from the Java source
repository into a set of C++ source repositories, which can be
released independently of the Java implementation. The Common, Files
and QtWidgets components were split out into separate repositories,
using `git filter-branch` to retain the history. The XML component
continues to reside in `bioformats.git` for the moment, but will also
be split out in a future release. The components may be built
individually, or as a collection using the OME Super-Build. This
release is a first step in our strategy to build a native reference
implementation for the OME Data Model, OME-TIFF, and hopefully more
modern formats like HDF5, etc.

Major changes include:

* Support for transparent model upgrades and downgrades when reading
  an OME-TIFF.
* Splitting of the codebase into a separate repository for each
  component.

## Source changes

* Move `cpp/` directory to top level.
* Rename all use of bioformats to ome-files.
* Use external gtest.
* Use CMake imported targets.
* Generate exported CMake configuration.
* Replace `bf_` prefixes with `ome_`.
* Get version information from git.
* Install using relative paths.
* Use doxygen tag files from ome-common and ome-xml.
* Use enum value list macros to simplify model enum switch statements.
* Add `source-archive` script to generate a source release.
